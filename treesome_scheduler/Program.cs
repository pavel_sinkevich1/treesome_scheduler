﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace treesome_scheduler
{
    class Program
    {
        public struct TreesomeCombination
        {
            public int Master;
            public int Slave1;
            public int Slave2;
            public TreesomeCombination(int master, int slave1, int slave2)
            {
                Master = master;
                Slave1 = slave1;
                Slave2 = slave2;
            }
            public override string ToString()
            {
                return string.Format("Master: {0}, Slave1: {1}, Slave2: {2}", Master.ToString(), Slave1.ToString(), Slave2.ToString());
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Total people count?");
            int peopleCount = 0;
            bool success = false;
            while (!success)
            {
                success = Int32.TryParse(Console.ReadLine(), out peopleCount);
                if (!success)
                {
                    Console.WriteLine("Value not valid");
                }
            }
            Console.WriteLine("How many laps?");
            int lapCount = 0;
            success = false;
            while (!success)
            {
                success = Int32.TryParse(Console.ReadLine(), out lapCount);
                if (!success)
                {
                    Console.WriteLine("Value not valid");
                }
            }

            /*
             * никто не выходит на сцену два раза подряд (первый и последний выход это тоже пара)
            каждый был ведущим ровно один раз
            одна и та же пара ведомых ни разу не повторилась
            каждый был ведомым одинаковое количество раз (2 раза)
             */
            List<TreesomeCombination> schedule = new List<TreesomeCombination>();
            // Step 1: generate list of Master
            var availablePeople = Enumerable.Range(1, peopleCount).ToList();
            Random rnd = new Random();
            for (int i = 0; i < peopleCount; i++)
            {
                int r = rnd.Next(availablePeople.Count);
                schedule.Add(new TreesomeCombination(availablePeople[r], -1, -1));
                availablePeople.RemoveAt(r);
            }
            if (lapCount > 1)
                for (int i1 = 1; i1 < lapCount; i1++)
                {
                    success = false;
                    List<int> sch1 = new List<int>();
                    while (!success)
                    {
                        success = true;
                        availablePeople = Enumerable.Range(1, peopleCount).ToList();
                        sch1.Clear();
                        for (int i = 0; i < peopleCount; i++)
                        {
                            int r = rnd.Next(availablePeople.Count);
                            sch1.Add(availablePeople[r]);
                            availablePeople.RemoveAt(r);
                        }
                        if (schedule[0].Master == sch1[peopleCount - 1] || schedule[schedule.Count - 1].Master == sch1[0])
                            success = false;
                    }
                    foreach(int i in sch1)
                        schedule.Add(new TreesomeCombination(i, -1, -1));

                }

            // Step 2: generate list of Slave1
            bool globalRestartRequired = true;
            while (globalRestartRequired)
            {
                globalRestartRequired = false;
                bool restartRequired = true;
                while (restartRequired)
                {
                    restartRequired = false;
                    for (int i1 = 0; i1 < lapCount; i1++)
                    {
                        availablePeople = Enumerable.Range(1, peopleCount).ToList();
                        for (int i = 0; i < peopleCount; i++)
                        {
                            List<int> neighboursList = new List<int>();
                            neighboursList.Add(schedule[i + i1*peopleCount].Master);
                            if (i + i1*peopleCount == 0)
                            {
                                neighboursList.Add(schedule[schedule.Count - 1].Master);
                            }
                            else
                            {
                                neighboursList.Add(schedule[i + i1*peopleCount - 1].Master);
                                neighboursList.Add(schedule[i + i1*peopleCount - 1].Slave1);
                            }
                            if (i + i1*peopleCount == schedule.Count - 1)
                            {
                                neighboursList.Add(schedule[0].Master);
                                neighboursList.Add(schedule[0].Slave1);
                            }
                            else
                            {
                                neighboursList.Add(schedule[i + i1*peopleCount + 1].Master);
                            }
                            int currentPerson = schedule[i + i1*peopleCount].Master;
                            int r = -1;
                            int attemptsLeft = peopleCount;
                            while (neighboursList.Contains(currentPerson) && !restartRequired)
                            {
                                r = rnd.Next(availablePeople.Count);
                                currentPerson = availablePeople[r];
                                attemptsLeft--;
                                if (attemptsLeft == 0)
                                    restartRequired = true;
                            }
                            if (restartRequired)
                                break;
                            schedule[i + i1*peopleCount] = new TreesomeCombination(schedule[i + i1*peopleCount].Master, currentPerson, -1);
                            availablePeople.RemoveAt(r);
                        }
                    }
                }
                // Step 3: generate list of Slave2
                int globalAttemptsLeft = peopleCount;
                restartRequired = true;
                while (restartRequired && globalAttemptsLeft > 0)
                {
                    restartRequired = false;
                    globalAttemptsLeft--;
                    for (int i1 = 0; i1 < lapCount; i1++)
                    {
                        availablePeople = Enumerable.Range(1, peopleCount).ToList();
                        for (int i = 0; i < peopleCount; i++)
                        {
                            List<int> neighboursList = new List<int>();
                            neighboursList.Add(schedule[i + i1*peopleCount].Master);
                            neighboursList.Add(schedule[i + i1*peopleCount].Slave1);
                            if (i + i1*peopleCount == 0)
                            {
                                neighboursList.Add(schedule[schedule.Count - 1].Master);
                                neighboursList.Add(schedule[schedule.Count - 1].Slave1);
                            }
                            else
                            {
                                neighboursList.Add(schedule[i + i1*peopleCount - 1].Master);
                                neighboursList.Add(schedule[i + i1*peopleCount - 1].Slave1);
                                neighboursList.Add(schedule[i + i1*peopleCount - 1].Slave2);
                            }
                            if (i + i1*peopleCount == schedule.Count - 1)
                            {
                                neighboursList.Add(schedule[0].Master);
                                neighboursList.Add(schedule[0].Slave1);
                                neighboursList.Add(schedule[0].Slave2);
                            }
                            else
                            {
                                neighboursList.Add(schedule[i + i1*peopleCount + 1].Master);
                                neighboursList.Add(schedule[i + i1*peopleCount + 1].Slave1);
                            }
                            int currentPerson = schedule[i + i1*peopleCount].Master;
                            int r = -1;
                            int attemptsLeft = peopleCount;
                            bool duplicatedCombination = false;
                            // пока всё плохо (в списке соседей есть данный человек, ИЛИ такая комбинация уже была)
                            // но не настолько плохо, чтобы был нужен рестарт
                            while ((neighboursList.Contains(currentPerson) || duplicatedCombination) && !restartRequired)
                            {
                                duplicatedCombination = false;
                                r = rnd.Next(availablePeople.Count);
                                currentPerson = availablePeople[r];
                                for (int i2 = 0; i2 < i + i1*peopleCount; i2++)
                                {
                                    if (schedule[i2].Slave1 == currentPerson && schedule[i2].Slave2 == schedule[i + i1*peopleCount].Slave1)
                                        duplicatedCombination = true;
                                }
                                attemptsLeft--;
                                if (attemptsLeft == 0)
                                    restartRequired = true;
                            }
                            if (restartRequired)
                                break;
                            schedule[i + i1*peopleCount] = new TreesomeCombination(schedule[i + i1*peopleCount].Master, schedule[i + i1*peopleCount].Slave1, currentPerson);
                            availablePeople.RemoveAt(r);
                        }
                    }
                }
                if (globalAttemptsLeft == 0)
                    globalRestartRequired = true;
            }
            schedule.ForEach(i => Console.WriteLine("{0}", i));
        }
    }
}
